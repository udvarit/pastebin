# ioQuake3 server cheat sheet

## commands

|command|effect|
|--|--|
|`\callvote map q3dm9`|Call a vote to change the map to q3dm9|
|`\callvote fraglimit 10`|Call a vote to set frag limit to 10|

## maps
### most popular
|map|size|clue|
|--|--|--|
|q3dm6|mid|quad is on the bottom and you jump up to a rocket launcher|
|q3dm9|mid|lava protects the rocket launcher|
|q3dm7|mid|push a button for the red shield|
|q3dm8|mid|either a quad or an invisibility spawns|
|hope|mid|quad sticks out of a corner|
|q3dm4|small||
|q3tourney4|small|
|q3dm12|mid-large|the one with the bfg room|
### runner-ups
fff
lun3_20b1
lun3dm1
lun3dm2
nodm5
uozq3dm4
ztn3dm1
ztn3dm2
### all maps alphabetically
aedm7
alm3dm4v2
auh3dm1
auh3dm2
bal3dm2
bal3dm3
bbq3dm1
bst3dm1
cf
ct3dm4
devdm3
efdatdm3
estatica
fff
fr3dm1
gm3tourney2
gs3dm5
haunted
hdm2
hope
ik3dm1
katdm3
lloydmdm2
lun3_20b1
lun3dm1
lun3dm2
lun3dm5
mrcq3t3
mrh_dm2
natedm2
nodm5
outpost
phantq3dm3_rev
phantq3dm4
pom_bots
q3dm17+
q3dm17++
q3ds
q3gwdm2
q3shw26
qxdm4_v2
riscq3dm2
rota3dm1
rustgrad
s20dm4
senndm2
shortcircuit
tig_den
uozq3dm4
ztn3dm1
ztn3dm2

